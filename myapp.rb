require "sinatra"

  post '/users' do
    'Usuario creado'
  end
  
  get '/users/:id' do
    "Usuario con id: #{params[:id]}"
  end
  
  get '/users' do
    'List of users'
  end
  
  put '/users/:id' do
    "Usuario reemplazado con id #{params[:id]}"
  end

  patch '/users/:id' do
    "Usuario actualizado con id #{params[:id]}"
  end

  delete '/users/:id' do
    "Usuario eliminado con id #{params[:id]}"
  end
  
  set :port, 4500
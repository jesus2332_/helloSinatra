
# Web Platforms 7CC2
# Esta tarea fue elaborada por: 
### Jesus Manuel Calleros Vazquez 348737
### Jose Eduardo Conde Hernandez 299506
### Miriam Fernanda Arellanes Perez 353256
## Esta tarea fue elaborada en Ruby acompañado de Sinatra, para poder ejecutar lo aprendido en clase sobre Micro web frameworks.
## En este caso es una aplicación web sencilla para gestionar usuarios. Proporciona las siguientes funcionalidades básicas:
### -Crear un usuario mediante POST '/users'
### -Obtener la información dde un usuario específico utilizando una solicitud GET '/users/:id'
### -Obtener una lista de todos los usuarios mediante GET '/users'
### -Actualizar un usuario existente utilizando una solicitud PUT '/users/:id'
### -Actualizar parcialmente a un usuario usando PATCH '/users/:id'
### -Eliminar un usuario mediante DELETE '/users/:id'

# Configuración del puerto:
### Dentro de nuestro 'myapp.rb' hay una linea que pone "set :port, 4500"
### Esta linea puede ser cambiada si es necesaria, cambiando el puerto utilizado.